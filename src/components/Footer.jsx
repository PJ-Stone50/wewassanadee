import React from 'react'

function Footer() {
  return (
    <div className='w-full flex flex-col p-3 text-white justify-center items-center' style={{backgroundColor: '#313233'}}>
        <ul className='flex gap-2'>
            <li className='hover:cursor-pointer'>Privacy Statement |</li>
            <li className='hover:cursor-pointer'>Terms and Conditions |</li>
            <li className='hover:cursor-pointer'>Imprint</li>
        </ul>
        <p className='opacity-60'>© 2023Taglinehere Thailand Co.,Ltd</p>
    </div>
  )
} 
export default Footer