import Image from 'next/image'
import { Inter } from 'next/font/google'

import HomePage from './HomePage'
import Navbar from '@/components/Navbar'
import Footer from '@/components/Footer'

import Link from 'next/link'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <main className="flex flex-col">
      <Navbar/>

      <HomePage />
      <Footer />
    </main>
  )
}
