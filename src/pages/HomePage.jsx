import React from 'react'
import Link from 'next/link'

import { useState } from 'react'
import { AiOutlineCheck } from "react-icons/ai";


const options = [
  { value: 'province', label: 'Province' },
  { value: 'city', label: 'City' },
  { value: 'zipcode', label: 'Zipcode' }
]


function HomePage() {
  const [isDoneStep1, setDoneStep1 ] = useState('0')
  const [isDoneStep2, setDoneStep2 ] = useState(false)

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [address, setAddress] = useState('')
  const [province, setProvince] = useState('province')
  const [city, setCity] = useState('city')
  const [zipcode, setZipcode] = useState('10400')


  const [age, setAge] = useState(0)
  const [ageScope, setAgeScope] = useState('')

  const [userName, setUserName] = useState('')
  const [password, setPassword] = useState('')
  
  const [firstCharacterName, setFirstCharacterName] = useState(firstName.slice(0,1))

  const [currentMenu, setCurrentMenu] = useState('')

  console.log(firstName)
  console.log("First C",firstCharacterName)
  console.log("AgeScope",ageScope)

  // const []

  const handleSubmit = () => {
    console.log("-------------",isDoneStep1)
    setFirstCharacterName(firstName.slice(0,1))
    setDoneStep1('1')
    console.log("Province-------------",province)
  }
  const handleSubmit2 = () => {
    // alert("Register successfully")
    console.log("-------------",isDoneStep1)
    setDoneStep1('2')
    setCurrentMenu('Profile')
  }
  const handleCancel = () => {
    console.log("+++++++++++++",isDoneStep1)
    setDoneStep1('0')
  }

  console.log(userName)
  console.log(password)

  return (
    <div className='h-full'>
      <div className="toggle-bar py-2 px-10 bg-red-700 w-full flex gap-1 px-3 text-white">
        <Link href="/" className=' hover:text-white hover:opacity-100  opacity-50'>Home</Link>/
        
        {currentMenu ? <Link href="/" className=' hover:text-white hover:opacity-100'>Profile</Link> : <Link href="/register" className=' hover:text-white hover:opacity-100'>Register</Link>}
      </div>

      
      {/* Step 1 & 2 */}
      <div className=" toggle-bar bg-red-200 w-full flex justify-center items-center text-white p-3 gap-5">
        {isDoneStep1 ? 
          <label className='items-center text-center items-center flex justify-center rounded-full border border-red-200 border-[5px] shadow w-fit  p-3 bg-red-700'><AiOutlineCheck/></label>
          : 
          <label className='items-center font-extrabold text-center items-center flex justify-center rounded-full border border-red-200 border-[5px] shadow w-[50px]  py-3 px-6 bg-red-700'>1</label> 
        }
        
        
        <label className='items-center text-center pr-[5rem]  w-[100px] h-[2px] bg-red-700'></label>
        <label className='items-center font-extrabold text-center rounded-full border border-red-700 shadow w-[50px]  p-3 bg-red-200 text-red-700'>2</label>
      </div>


      <main className=' w-full h-full pt-10 pb-20 flex justify-center items-center '>
        {/* Step1 */}
        {isDoneStep1 === '0'  && 
        <form onSubmit={handleSubmit} className="w-full gap-5 max-w-[40rem] shadow-lg rounded flex flex-col items-center  relative p-5">
          <h1 className='text-start text-[32px] uppercase font-bold w-full'>Register</h1>

          <div className=" w-full input-name flex flex-col    items-center justify-center">
            <div className="flex flex-col gap-2 w-full  ">
              <h3 className='uppercase  text-[18px]' style={{fontWeight: '600'}}>Your name</h3>
              <div className="relative flex gap-2 mb-3  w-full" data-te-input-wrapper-init>
                <div className="relative w-full">
                  <input
                    type="text"
                    className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                    id="exampleFormControlInput50"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                    />
                  <label
                    htmlFor="exampleFormControlInput50"
                    className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-neutral-500 transition-all duration-200 ease-out  peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                    >{firstName.length <= 0 && <p>Firstname</p>}
                  </label>
                </div>
                <div className='relative w-full' >
                <input
                  type="text"
                  className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                  id="lname"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  required
                  />
                <label
                  htmlFor="lname"
                  className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                  >{lastName.length <= 0 && <p>Lastname</p>}
                </label>
                </div>
              </div>
            </div>

          </div>
          {/* -*----- */}
          <div className=" w-full input-address flex flex-col    items-center justify-center">
            <div className="flex flex-col gap-2 w-full  min-w-[400px]">
              <h3 className='uppercase text-[18px]'  style={{fontWeight: '600'}}>Address</h3>
              <div className="relative flex flex-col gap-2 mb-3 " data-te-input-wrapper-init>
                  <div className='relative w-full' >
                    <input
                      type="text"
                      className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                      id="address"
                      value={address}
                      onChange={(e) => setAddress(e.target.value)}
                      />
                    <label
                      htmlFor="address"
                      className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-black transition-all duration-200 ease-out peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                      >{address.length <= 0 && <p>Address</p>}
                    </label>
                  </div>
                  


                  <select defaultValue="province" onChange={(e) => setProvince(e.target.value)} name="" id="" className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0">
                    <option  value="province">Province</option>
                  </select>

                  <div className="flex gap-2">
                    <select name="" id="" className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0">
                      <option value="">City</option>
                    </select>
                    <select name="" id="" className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0">
                      <option value="">Zip code</option>
                    </select>
                  </div>
                
              </div>
            </div>

          </div>
          {/* ---- */}

          <div className=" w-full input-address flex flex-col    items-center justify-center">
            <div className="flex flex-col gap-2 w-full  min-w-[400px]">
              <h3 className='uppercase  text-[18px]'  style={{fontWeight: '600'}}>Age</h3>
              <div className="relative flex flex-col gap-2 mb-3  " data-te-input-wrapper-init>
                <div className="flex flex-col" onChange={(e) => setAgeScope(e.target.value)}>
                  <div className="flex gap-2">
                    <input type="radio" defaultChecked='18-25' name="age" value='18-25' placeholder='18-25'/>
                    <label htmlFor="18years">18-25</label>
                  </div>
                  <div className="flex gap-2">
                    <input type="radio" name="age" value='26-30' placeholder='26-30'/>
                    <label htmlFor="18years">26-30</label>
                  </div>
                  <div className="flex gap-2">
                    <input type="radio" name="age" value='Other' />
                    <label htmlFor="18years">Other</label>
                  </div>
                </div>
                  
                <div className='relative w-full' >
                    <input
                      type="text"
                      className="border border-stone-300 peer block min-h-[auto] w-1/2 rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                      id="age"
                      
                      onChange={(e) => setAge(e.target.value)}
                      />
                    <label
                      htmlFor="age"
                      className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-black transition-all duration-200 ease-out peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                      >Age
                    </label>
                  </div>
                
              </div>
            </div>

          </div>

            <div className="input-submit  flex gap-2 w-full justify-end">
              <button type="button" className='rounded-lg px-3 py-1 border border-red-500 text-red-500' onClick={() => console.log('cancel')}>Cancel</button>
              <button type='submit' className='rounded-lg px-3 py-1 bg-slate-700 text-white'>Next</button>
            </div>
          </form>
      // Step2
    }

        {isDoneStep1 === '1' &&
          <div className='w-full  gap-5 max-w-[40rem] mb-[360px] shadow-lg rounded flex flex-col  relative p-5 '>
                <form onSubmit={handleSubmit2} className="flex flex-col  items-center justify-start relative h-full">
                  <h1 className='text-start text-[32px] uppercase font-bold w-full'>Register</h1>

                  <div className="input-name flex flex-col mt-[50px] w-full mb-5 items-center justify-center">
                    <div className="flex flex-col gap-2 w-full  min-w-[400px]">
                      <h3 className='uppercase font-semibold'>Login details</h3>
                      <div className="input-name flex gap-5 w-full items-center justify-center">

                      <div className="relative w-full">
                        <input
                          type="text"
                          className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                          id="username"
                          value={userName}
                          onChange={(e) => setUserName(e.target.value)
                          }
                          />
                        <label
                          htmlFor="username"
                          className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-neutral-500 transition-all duration-200 ease-out  peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                          >{userName.length <= 0 && <p>Username</p>}
                        </label>
                      </div>
                      <div className="relative w-full">
                        <input
                          type="text"
                          className="border border-stone-300 peer block min-h-[auto] w-full rounded  bg-white px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-primary data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:bg-neutral-700 dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-primary [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                          id="password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)
                          }
                          />
                        <label
                          htmlFor="password"
                          className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-neutral-500 transition-all duration-200 ease-out  peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-primary"
                          >{password.length <= 0 && <p>Password</p>}
                        </label>
                      </div>
                      

                      </div>
                    </div>
                  </div>



                  <div className="input-submit  flex gap-2 w-full justify-end">
                    <button className='rounded-lg px-3 py-1 border border-red-500 text-red-500' onClick={handleCancel}>Back</button>
                    <button type='submit' className='rounded-lg px-3 py-1 bg-slate-700 text-white' >Register</button>
                    
                      
                  </div>
                </form>
          </div>
        }
        {isDoneStep1=== '2' &&
          <div className='final-step w-full mb-[150px] max-w-[872px] px-[220px] shadow-lg rounded flex flex-col items-center relative p-5 text-black'>
                <div className="input-submit w-fit flex gap-2 justify-end absolute top-0 left-0 m-5">
                    <button className='rounded-lg px-3 py-1 border border-black text-black hover:bg-black hover:text-white duration-300' onClick={() => {setDoneStep1('1');setCurrentMenu('')}}>Back</button>
                </div>
                
                <label htmlFor="" className='bg-red-200 rounded-full h-[200px] w-[200px] items-center justify-center flex text-[99px] uppercase' style={{color:'#A81E24',fontWeight:'600'}}>{firstCharacterName}</label>

                <h3 className='text-[32px]' style={{fontWeight: '600'}}>{firstName} {lastName}</h3>
                <p className='text-[18px]  opacity-40' style={{fontWeight: '400'}}>{age} years</p>

                <label htmlFor="" className='h-[1px] w-[424px] bg-stone-500 my-3 opacity-40'></label>

                <div className="relative flex flex-col gap-2 mb-3  w-full mt-5" data-te-input-wrapper-init>
                  <h3 h3 className='uppercase  text-[18px] whitespace-nowrap' style={{fontWeight: '600'}}>Address</h3>
                  <div className="w-full">
                      <p>{address}</p>
                  </div>
                </div>

                <div className="relative flex flex-col gap-2 mb-3  w-full mt-5" data-te-input-wrapper-init>
                  <h3 h3 className='uppercase  text-[18px] whitespace-nowrap' style={{fontWeight: '600'}}>Login Details</h3>
                  <div className="w-full flex gap-[150px] ">
                      <span className='relative' style={{fontWeight: '400'}}>
                      <p className='opacity-40'>Username</p>
                        <label htmlFor="" className='absolute bottom-[-30px] left-0 opacity-100'>{userName}</label>
                      </span>
                      <span className='relative ' style={{fontWeight: '400'}}>
                      <p className='opacity-40'>Password</p>
                        <label htmlFor="" className='absolute bottom-[-30px] left-0 opacity-100'>{password}</label>
                      </span>
                  </div>
                </div>

                <div className="flex pt-[1rem] pb-[1rem] gap-5">
                  <button className='rounded-full px-[40px] py-2 text-[18px] border border-stone-800 text-stone-800 bg-white'>Log out</button>
                  <button className='rounded-full px-[40px] py-2 text-[18px] text-white bg-stone-800'>Log out</button>
                </div>


          </div>
        }
      



      </main>
    </div>
  )
}

export default HomePage