import React from 'react'
import Image from "next/image";
import Link from 'next/link'

function Navbar() {
  return (
    <div className='bg-white w-full flex justify-between py-3 px-[100px]'>
        

        <div className="logo w-fit">
            <Image src='/logo1.png' alt='logo-image' width={112} height={52}/>
        </div>
        <div className="menubar w-fit gap-5 flex items-center text-stone-600">
    
            <Link href="/" className=' hover:text-black'>Home</Link>
            <p>Menu1</p>
            <p>Menu2</p>
            <p>Menu3</p>
            <p>Menu4</p>
        </div>
    </div>
  )
}

export default Navbar